# decoro

#### 项目介绍
- 项目名称：decoro
- 所属系列：openharmony的第三方组件适配移植
- 功能：库设计的自动格式化文本输入自定义规则。
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 Beta1
- 基线版本：master分支

#### 效果演示
<img src="img/gif.gif"></img>

#### 安装教程

1.在项目根目录下的build.gradle文件中，
 ```
 allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
 }
```
2.在entry模块的build.gradle文件中，
 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:decoro:1.0.0')
    ......
 }
```

#### 使用说明

String格式，在下面的示例中，字符串的内容将根据Mask格式化。

示例 1，使用预先定义的Mask格式化

```
 Mask mask = new MaskImpl(PredefinedSlots.RUS_PHONE_NUMBER, true);
 mask.insertFront("9995554433");
 System.out.println(mask.toString()); // +7 (999) 555-44-33
```

示例 2，用户的文本输入将按照MaskImpl进行格式化

```
MaskImpl mask = MaskImpl.createTerminated(PredefinedSlots.CARD_NUMBER_USUAL);
FormatWatcher watcher = new MaskFormatWatcher(mask);
watcher.installOn(editText); // install on any TextView
```

library中提供的Mask格式化：

```
PredefinedSlots.SINGLE_SLOT                   // Any character
PredefinedSlots.RUS_PHONE_NUMBER              // Russian phone number formatted as +7 (___) ___-__-__ (digits only)
PredefinedSlots.RUS_PASSPORT                  // Series and number of russian passport formatted as ____ ______  (digits only)
PredefinedSlots.CARD_NUMBER_STANDARD          // Credit card number formatted as ____ ____ ____ ____ (digits only)
PredefinedSlots.CARD_NUMBER_STANDARD_MASKABLE // Credit card number formatted as ____ ____ ____ ____ (digits and chars 'X', 'x', '*')
PredefinedSlots.CARD_NUMBER_MAESTRO           // Credit card number formatted as ________ ____ (digits only)
PredefinedSlots.CARD_NUMBER_MAESTRO_MASKABLE  // Credit card number formatted as ________ ____ (digits and chars 'X', 'x', '*')
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异

#### 版本迭代

- 1.0.0

#### 版权和许可信息

Apache License 2.0