package ru.tinkoff.decoro.demo;

import ru.tinkoff.decoro.demo.slice.CustomMaskAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CustomMaskAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CustomMaskAbilitySlice.class.getName());
    }
}
