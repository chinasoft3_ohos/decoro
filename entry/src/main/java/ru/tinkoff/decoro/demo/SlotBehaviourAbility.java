package ru.tinkoff.decoro.demo;

import ru.tinkoff.decoro.demo.slice.SlotBehaviourAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class SlotBehaviourAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(SlotBehaviourAbilitySlice.class.getName());
    }
}
