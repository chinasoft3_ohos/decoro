package ru.tinkoff.decoro.demo;

import ru.tinkoff.decoro.demo.slice.StaticMaskAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class StaticMaskAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(StaticMaskAbilitySlice.class.getName());
    }
}
