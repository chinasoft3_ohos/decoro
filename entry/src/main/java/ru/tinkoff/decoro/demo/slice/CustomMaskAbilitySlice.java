package ru.tinkoff.decoro.demo.slice;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ru.tinkoff.decoro.MaskImpl;
import ru.tinkoff.decoro.TextUtils;
import ru.tinkoff.decoro.demo.ResourceTable;
import ru.tinkoff.decoro.parser.SlotsParser;
import ru.tinkoff.decoro.parser.UnderscoreDigitSlotsParser;
import ru.tinkoff.decoro.slots.PredefinedSlots;
import ru.tinkoff.decoro.slots.Slot;
import ru.tinkoff.decoro.watchers.MaskFormatWatcher;

public class CustomMaskAbilitySlice extends AbilitySlice {

    private TextField dataEdit;
    private Text hint;
    private MaskFormatWatcher formatWatcher;
    private SlotsParser slotsParser = new UnderscoreDigitSlotsParser();
    private String textBeforeChange = "";

    private Text.TextObserver maskTextWatcher = new Text.TextObserver() {

        @Override
        public void onTextUpdated(String str, int i, int i1, int i2) {
            final MaskImpl maskDescriptor;
            if (TextUtils.isEmpty(str)) {
                maskDescriptor = createEmptyMask();
            } else {
                maskDescriptor = MaskImpl.createTerminated(slotsParser.parseSlots(str));
                maskDescriptor.setHideHardcodedHead(false);
                maskDescriptor.insertFront(dataEdit.getText());
            }
            formatWatcher.setMask(maskDescriptor);
        }
    };

    private MaskImpl createEmptyMask() {
        return MaskImpl.createNonTerminated(new Slot[]{PredefinedSlots.any()});
    }

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_custom_mask);

        TextField maskEdit = (TextField) findComponentById(ResourceTable.Id_editMask);
        maskEdit.addTextObserver(maskTextWatcher);
        formatWatcher = new MaskFormatWatcher(createEmptyMask());
        dataEdit = (TextField) findComponentById(ResourceTable.Id_editData);

        hint = (Text) findComponentById(ResourceTable.Id_editHint);
        formatWatcher.installOn(dataEdit);
        dataEdit.setFocusChangedListener(new Component.FocusChangedListener() {
            @Override
            public void onFocusChange(Component component, boolean isFocus) {
                if (isFocus) {
                    hint.setVisibility(Component.VISIBLE);
                    if (TextUtils.isEmpty(dataEdit.getText())) {
                        dataEdit.setHint("");
                    } else {
                        dataEdit.setHint(getString(ResourceTable.String_hint_data));
                    }
                } else {
                    if (TextUtils.isEmpty(dataEdit.getText())) {
                        hint.setVisibility(Component.INVISIBLE);
                        dataEdit.setHint(getString(ResourceTable.String_hint_data));
                    } else {
                        hint.setVisibility(Component.VISIBLE);
                    }
                }
            }
        });
        dataEdit.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String newText, int i, int i1, int i2) {
                if (TextUtils.isEmpty(newText)) {
                    hint.setVisibility(Component.INVISIBLE);
                    dataEdit.setHint(getString(ResourceTable.String_hint_data));
                } else {
                    hint.setVisibility(Component.VISIBLE);
                    dataEdit.setHint(getString(ResourceTable.String_hint_data));
                }
                String maskText = maskEdit.getText();
                if (!TextUtils.isEmpty(maskText)) {
                    MaskImpl maskDescriptor = MaskImpl.createTerminated(slotsParser.parseSlots(maskText));
                    maskDescriptor.setHideHardcodedHead(false);

                    if (textBeforeChange.length() < newText.length()) {
                        maskDescriptor.insertFront(dataEdit.getText());
                        formatWatcher.setMask(maskDescriptor);
                    }
                    int length = maskText.length();
                    if (length <= textBeforeChange.length()){
                        maskDescriptor.insertFront(dataEdit.getText());
                        formatWatcher.setMask(maskDescriptor);
                    }
                }
                textBeforeChange = newText;
            }
        });
        MaskImpl maskDescriptor = createEmptyMask();
        formatWatcher.setMask(maskDescriptor);
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}