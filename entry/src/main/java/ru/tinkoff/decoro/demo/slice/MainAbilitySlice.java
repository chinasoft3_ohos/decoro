package ru.tinkoff.decoro.demo.slice;

import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ru.tinkoff.decoro.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;

public class MainAbilitySlice extends AbilitySlice implements Component.ClickedListener {

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        findComponentById(ResourceTable.Id_btnCustomMask).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnStaticMask).setClickedListener(this);
        findComponentById(ResourceTable.Id_btnSlotsBehaviour).setClickedListener(this);
    }

    @Override
    public void onClick(Component component) {
        Intent intent = new Intent();
        switch (component.getId()) {
            case ResourceTable.Id_btnCustomMask:
                Operation operation = new Intent.OperationBuilder()
                        .withBundleName("ru.tinkoff.decoro.demo")
                        .withAbilityName("ru.tinkoff.decoro.demo.CustomMaskAbility")
                        .build();
                intent.setOperation(operation);
                startAbility(intent);
                break;
            case ResourceTable.Id_btnStaticMask:
                Operation ot = new Intent.OperationBuilder()
                        .withBundleName("ru.tinkoff.decoro.demo")
                        .withAbilityName("ru.tinkoff.decoro.demo.StaticMaskAbility")
                        .build();
                intent.setOperation(ot);
                startAbility(intent);
                break;
            case ResourceTable.Id_btnSlotsBehaviour:
                Operation or = new Intent.OperationBuilder()
                        .withBundleName("ru.tinkoff.decoro.demo")
                        .withAbilityName("ru.tinkoff.decoro.demo.SlotBehaviourAbility")
                        .build();
                intent.setOperation(or);
                startAbility(intent);
                break;
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}