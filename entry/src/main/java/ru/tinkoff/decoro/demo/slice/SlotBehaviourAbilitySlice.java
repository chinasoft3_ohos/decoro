package ru.tinkoff.decoro.demo.slice;

import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ru.tinkoff.decoro.MaskImpl;
import ru.tinkoff.decoro.demo.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ru.tinkoff.decoro.slots.PredefinedSlots;
import ru.tinkoff.decoro.slots.Slot;
import ru.tinkoff.decoro.slots.ValueInterpreter;
import ru.tinkoff.decoro.watchers.MaskFormatWatcher;

public class SlotBehaviourAbilitySlice extends AbilitySlice {

    private TextField slot0;
    private TextField slot1;
    private TextField slot2;
    private TextField slot3;
    private TextField slot4;
    private String slot0BeforeChange = "";
    private String slot1BeforeChange = "";
    private String slot2BeforeChange = "";
    private String slot3BeforeChange = "";
    private String slot4BeforeChange = "";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_slot_behaviour);

        slot0 = (TextField) findComponentById(ResourceTable.Id_slot0);
        slot1 = (TextField) findComponentById(ResourceTable.Id_slot1);
        slot2 = (TextField) findComponentById(ResourceTable.Id_slot2);
        slot3 = (TextField) findComponentById(ResourceTable.Id_slot3);
        slot4 = (TextField) findComponentById(ResourceTable.Id_slot4);

        initSlot0(new ExampleBehaviour() {
            @Override
            void changeMask(MaskImpl mask) {
                mask.setHideHardcodedHead(true);
            }
        });
        initSlot1(new ExampleBehaviour() {
            @Override
            void changeSlots(Slot[] slots) {
                slots[3].setFlags(slots[3].getFlags() | Slot.RULE_FORBID_CURSOR_MOVE_LEFT);
            }
        });
        initSlot2(new ExampleBehaviour() {
            @Override
            void changeSlots(Slot[] slots) {
                slots[1].setValueInterpreter(new ValueInterpreter() {
                    @Override
                    public Character interpret(Character character) {
                        if (character == null) {
                            return null;
                        }
                        return character == '8' ? '7' : character;
                    }
                });
            }
        });
        initSlot3(new ExampleBehaviour() {
            @Override
            boolean fillWhenInstall() {
                return true;
            }

            @Override
            void changeSlots(Slot[] slots) {
                slots[3].setFlags(slots[3].getFlags() | Slot.RULE_FORBID_CURSOR_MOVE_LEFT);
            }
        });
        initSlot4(new ExampleBehaviour() {
            @Override
            boolean fillWhenInstall() {
                return true;
            }

            @Override
            void changeSlots(Slot[] slots) {
                slots[3].setFlags(slots[3].getFlags() | Slot.RULE_FORBID_CURSOR_MOVE_LEFT);
            }
        });
    }

    private void init(TextField editText, ExampleBehaviour behaviour) {
        Slot[] slots = Slot.copySlotArray(PredefinedSlots.getRusPhoneNumber());
        behaviour.changeSlots(slots);
        MaskImpl mask = new MaskImpl(slots, behaviour.isTerminated());
        behaviour.changeMask(mask);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        if (behaviour.fillWhenInstall()) {
            watcher.installOnAndFill(editText);
        } else {
            watcher.installOn(editText);
        }
    }

    private void initSlot0(ExampleBehaviour behaviour) {
        Slot[] slots = Slot.copySlotArray(PredefinedSlots.getRusPhoneNumber());
        behaviour.changeSlots(slots);
        MaskImpl mask = new MaskImpl(slots, behaviour.isTerminated());
        behaviour.changeMask(mask);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        if (behaviour.fillWhenInstall()) {
            watcher.installOnAndFill(slot0);
        } else {
            watcher.installOn(slot0);
        }
        slot0.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String newText, int ii, int i1, int i2) {
                if (slot0BeforeChange.length() < newText.length()){
                    watcher.refreshMask(newText);
                }
                int length = 18;
                if (length <= slot0BeforeChange.length()){
                    watcher.refreshMask(newText);
                }
                slot0BeforeChange = newText;
            }
        });
    }

    private void initSlot1(ExampleBehaviour behaviour) {
        Slot[] slots = Slot.copySlotArray(PredefinedSlots.getRusPhoneNumber());
        behaviour.changeSlots(slots);
        MaskImpl mask = new MaskImpl(slots, behaviour.isTerminated());
        behaviour.changeMask(mask);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        if (behaviour.fillWhenInstall()) {
            watcher.installOnAndFill(slot1);
        } else {
            watcher.installOn(slot1);
        }
        slot1.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String newText, int ii, int i1, int i2) {
                if (slot1BeforeChange.length() < newText.length()){
                    watcher.refreshMask(newText);
                }
                int length = 18;
                if (length <= slot1BeforeChange.length()){
                    watcher.refreshMask(newText);
                }
                if (newText.length() <= 4){
                    watcher.refreshMask(newText);
                }
                slot1BeforeChange = newText;
            }
        });
    }

    private void initSlot2(ExampleBehaviour behaviour) {
        Slot[] slots = Slot.copySlotArray(PredefinedSlots.getRusPhoneNumber());
        behaviour.changeSlots(slots);
        MaskImpl mask = new MaskImpl(slots, behaviour.isTerminated());
        behaviour.changeMask(mask);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        if (behaviour.fillWhenInstall()) {
            watcher.installOnAndFill(slot2);
        } else {
            watcher.installOn(slot2);
        }
        slot2.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String newText, int ii, int i1, int i2) {
                if (slot2BeforeChange.length() < newText.length()){
                    watcher.refreshMask(newText);
                }
                int length = 18;
                if (length <= slot2BeforeChange.length()){
                    watcher.refreshMask(newText);
                }
                if (newText.length() <= 4){
                    watcher.refreshMask(newText);
                }
                slot2BeforeChange = newText;
            }
        });
    }

    private void initSlot3(ExampleBehaviour behaviour) {
        Slot[] slots = Slot.copySlotArray(PredefinedSlots.getRusPhoneNumber());
        behaviour.changeSlots(slots);
        MaskImpl mask = new MaskImpl(slots, behaviour.isTerminated());
        behaviour.changeMask(mask);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        if (behaviour.fillWhenInstall()) {
            watcher.installOnAndFill(slot3);
        } else {
            watcher.installOn(slot3);
        }
        slot3.setText("+7 (");
        slot3.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String newText, int ii, int i1, int i2) {
                if (slot3BeforeChange.length() < newText.length()){
                    watcher.refreshMask(newText);
                }
                int length = 18;
                if (length <= slot3BeforeChange.length()){
                    watcher.refreshMask(newText);
                }
                if (newText.length() <= 4){
                    watcher.refreshMask(newText);
                }
                slot3BeforeChange = newText;
            }
        });
    }

    private void initSlot4(ExampleBehaviour behaviour) {
        Slot[] slots = Slot.copySlotArray(PredefinedSlots.getRusPhoneNumber());
        behaviour.changeSlots(slots);
        MaskImpl mask = new MaskImpl(slots, behaviour.isTerminated());
        behaviour.changeMask(mask);
        MaskFormatWatcher watcher = new MaskFormatWatcher(mask);
        if (behaviour.fillWhenInstall()) {
            watcher.installOnAndFill(slot4);
        } else {
            watcher.installOn(slot4);
        }
        slot4.addTextObserver(new Text.TextObserver() {

            @Override
            public void onTextUpdated(String newText, int ii, int i1, int i2) {
                if (slot4BeforeChange.length() < newText.length()){
                    watcher.refreshMask(newText);
                }
                int length = 18;
                if (length <= slot4BeforeChange.length()){
                    watcher.refreshMask(newText);
                }
                if (newText.length() <= 4){
                    watcher.refreshMask(newText);
                }
                slot4BeforeChange = newText;
            }
        });
    }

    static class ExampleBehaviour {
        void changeMask(MaskImpl mask) {
        }

        void changeSlots(Slot[] slots) {
        }

        boolean isTerminated() {
            return true;
        }

        boolean fillWhenInstall() {
            return false;
        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }
}
