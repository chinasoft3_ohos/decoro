/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain an copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.tinkoff.decoro.demo;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import java.lang.reflect.Method;

import static org.junit.Assert.assertEquals;

/**
 * 测试
 *
 * @author ljx
 * @since 2021-06-02
 */
public class ExampleOhosTest {
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("ru.tinkoff.decoro.demo", actualBundleName);
    }

    @Test
    public void getTrueMask() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.watchers.FormatWatcher");
            Method log = mainAbilitySlice.getMethod("getTrueMask");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getText() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.watchers.FormatWatcher");
            Method log = mainAbilitySlice.getMethod("getText");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getCursorPosition() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.watchers.FormatWatcher");
            Method log = mainAbilitySlice.getMethod("getCursorPosition");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getFirstSlot() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.SlotsList");
            Method log = mainAbilitySlice.getMethod("getFirstSlot");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getLastSlot() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.SlotsList");
            Method log = mainAbilitySlice.getMethod("getLastSlot");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getRusPhoneNumber() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.slots.PredefinedSlots");
            Method log = mainAbilitySlice.getMethod("getRusPhoneNumber");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getRusPassport() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.slots.PredefinedSlots");
            Method log = mainAbilitySlice.getMethod("getRusPassport");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getCardNumberStandardMaskable() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.slots.PredefinedSlots");
            Method log = mainAbilitySlice.getMethod("getCardNumberStandardMaskable");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getCardNumberMaestroMaskable() {
        try {
            Class  mainAbilitySlice = Class.forName("ru.tinkoff.decoro.slots.PredefinedSlots");
            Method log = mainAbilitySlice.getMethod("getCardNumberMaestroMaskable");
            Object obj = mainAbilitySlice.getConstructor().newInstance();
            log.invoke(obj);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}