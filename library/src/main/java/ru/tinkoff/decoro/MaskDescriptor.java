/*
 * Copyright © 2016 Tinkoff Bank
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.tinkoff.decoro;

import java.io.Serializable;
import java.util.Arrays;

import ohos.utils.Parcel;
import ohos.utils.Sequenceable;
import ru.tinkoff.decoro.slots.PredefinedSlots;
import ru.tinkoff.decoro.slots.Slot;

/**
 * @author Mikhail Artemev
 */
public class MaskDescriptor implements Serializable, Sequenceable {

    private Slot[] slots;
    private String rawMask;
    private String initialValue;
    private boolean terminated = true;
    private boolean forbidInputWhenFilled = false;
    private boolean hideHardcodedHead = false;

    public MaskDescriptor() {
    }

    public static MaskDescriptor ofRawMask(final String rawMask) {
        if (TextUtils.isEmpty(rawMask)) {
            return emptyMask();
        }
        return new MaskDescriptor().setRawMask(rawMask);
    }

    public static MaskDescriptor ofRawMask(final String rawMask, final boolean terminated) {
        return new MaskDescriptor()
                .setRawMask(rawMask)
                .setTerminated(terminated);
    }

    public static MaskDescriptor ofSlots(Slot[] slots) {
        return new MaskDescriptor().setSlots(slots);
    }

    public static MaskDescriptor emptyMask() {
        return new MaskDescriptor()
                .setSlots(new Slot[]{PredefinedSlots.any()})
                .setTerminated(false);
    }

    public MaskDescriptor(MaskDescriptor copy) {
        if (copy.slots != null) {
            this.slots = new Slot[copy.slots.length];
            System.arraycopy(copy.slots, 0, this.slots, 0, copy.slots.length);
        }

        this.rawMask = copy.rawMask;
        this.initialValue = copy.initialValue;

        this.terminated = copy.terminated;
        this.hideHardcodedHead = copy.hideHardcodedHead;
        this.forbidInputWhenFilled = copy.forbidInputWhenFilled;
    }

    protected MaskDescriptor(Parcel in) {
        this.slots = (Slot[]) in.createSequenceableArray();
        this.rawMask = in.readString();
        this.initialValue = in.readString();
        this.terminated = in.readByte() != 0;
        this.forbidInputWhenFilled = in.readByte() != 0;
        this.hideHardcodedHead = in.readByte() != 0;
    }

    public boolean isValid() {
        return slots != null || !TextUtils.isEmpty(rawMask);
    }

    public void validateOrThrow() {
        if (!isValid()) {
            throw new IllegalStateException("Mask descriptor is malformed. Should have at least slots array or raw mask (string representation)");
        }
    }

    @Deprecated
    public MaskDescriptor withSlots(Slot[] value) {
        slots = value.clone();
        return this;
    }

    public MaskDescriptor withRawMask(String value) {
        rawMask = value;
        return this;
    }

    public MaskDescriptor withInitialValue(String value) {
        initialValue = value;
        return this;
    }

    public MaskDescriptor withTermination(boolean value) {
        terminated = value;
        return this;
    }

    public MaskDescriptor withShowingEmptySlots(boolean value) {
        return this;
    }

    public MaskDescriptor withEmptySlotPalceholder(Character value) {
        return this;
    }

    public MaskDescriptor withForbiddingInputWhenFilled(boolean value) {
        forbidInputWhenFilled = value;
        return this;
    }

    /**
     * use {link {@link #setHideHardcodedHead(boolean)}} instead
     *
     * @param value boolean
     * @return MaskDescriptor
     */
    public MaskDescriptor withHideHardcodedHead(boolean value) {
        hideHardcodedHead = value;
        return this;
    }


    public Slot[] getSlots() {
        return slots.clone();
    }

    public MaskDescriptor setSlots(Slot[] slots) {
        this.slots = slots.clone();
        return this;
    }


    public String getRawMask() {
        return rawMask;
    }

    public MaskDescriptor setRawMask(String rawMask) {
        this.rawMask = rawMask;
        return this;
    }

    public boolean isTerminated() {
        return terminated;
    }

    public MaskDescriptor setTerminated(boolean terminated) {
        this.terminated = terminated;
        return this;
    }


    public String getInitialValue() {
        return initialValue;
    }

    public MaskDescriptor setInitialValue(String initialValue) {
        this.initialValue = initialValue;
        return this;
    }

    @Deprecated
    public boolean isShowEmptySlots() {
        return false;
    }

    @Deprecated
    public MaskDescriptor setShowEmptySlots(boolean showEmptySlots) {
        return this;
    }

    @Deprecated
    public Character getEmptySlotPlaceholder() {
        return '_';
    }

    @Deprecated
    public MaskDescriptor setEmptySlotPlaceholder(Character emptySlotPlaceholder) {
        return this;
    }

    public boolean isForbidInputWhenFilled() {
        return forbidInputWhenFilled;
    }

    public MaskDescriptor setForbidInputWhenFilled(boolean forbidInputWhenFilled) {
        this.forbidInputWhenFilled = forbidInputWhenFilled;
        return this;
    }

    public boolean isHideHardcodedHead() {
        return hideHardcodedHead;
    }

    public MaskDescriptor setHideHardcodedHead(boolean hideHardcodedHead) {
        this.hideHardcodedHead = hideHardcodedHead;
        return this;
    }

    @Override
    public boolean marshalling(Parcel dest) {
        dest.writeSequenceableArray(this.slots);
        dest.writeString(this.rawMask);
        dest.writeString(this.initialValue);
        dest.writeByte(this.terminated ? (byte) 1 : (byte) 0);
        dest.writeByte(this.forbidInputWhenFilled ? (byte) 1 : (byte) 0);
        dest.writeByte(this.hideHardcodedHead ? (byte) 1 : (byte) 0);
        return true;
    }

    @Override
    public boolean unmarshalling(Parcel parcel) {
        return false;
    }

    public static final Sequenceable.Producer<MaskDescriptor> PRODUCER = new Sequenceable.Producer<MaskDescriptor>() {

        public MaskDescriptor createFromParcel(Parcel in) {
            MaskDescriptor adrecordstore = new MaskDescriptor(in);
            adrecordstore.unmarshalling(in);
            return adrecordstore;
        }
    };

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        MaskDescriptor that = (MaskDescriptor) o;

        if (terminated != that.terminated) return false;
        if (forbidInputWhenFilled != that.forbidInputWhenFilled) return false;
        if (hideHardcodedHead != that.hideHardcodedHead) return false;
        if (!Arrays.equals(slots, that.slots)) return false;
        if (rawMask != null ? !rawMask.equals(that.rawMask) : that.rawMask != null) return false;
        return initialValue != null ? initialValue.equals(that.initialValue) : that.initialValue == null;

    }

    @Override
    public int hashCode() {
        int result = Arrays.hashCode(slots);
        result = 31 * result + (rawMask != null ? rawMask.hashCode() : 0);
        result = 31 * result + (initialValue != null ? initialValue.hashCode() : 0);
        result = 31 * result + (terminated ? 1 : 0);
        result = 31 * result + (forbidInputWhenFilled ? 1 : 0);
        result = 31 * result + (hideHardcodedHead ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        if (!TextUtils.isEmpty(rawMask)) {
            return rawMask;
        } else if (slots != null && slots.length > 0) {
            return slotsToString();
        }
        return "(empty)";
    }

    private String slotsToString() {
        final StringBuilder result = new StringBuilder(slots.length);
        for (Slot slot : slots) {
            Character value = slot.getValue();
            if (value == null) {
                value = Slot.PLACEHOLDER_DEFAULT;
            }
            result.append(value);
        }
        return result.toString();
    }
}
