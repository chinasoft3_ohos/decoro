/*
 * Copyright © 2016 Tinkoff Bank
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package ru.tinkoff.decoro.watchers;

import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ru.tinkoff.decoro.FormattedTextChangeListener;
import ru.tinkoff.decoro.Mask;
import ru.tinkoff.decoro.MaskFactory;
import ru.tinkoff.decoro.TextUtils;

/**
 * <p>
 * This class encapsulates logic of formatting (pretty printing) content of a Text. All
 * the formatting logic is encapsulated inside the {@link Mask} class. This class is only
 * used to follow Text changes and format it according to the {@link Mask}. It's okay
 * to
 * use it either with or. Important note for using with
 * bare. Since its content usually changes with,
 * inserting text should contain all the hardcoded symbols of the {@link Mask}.
 * <p>
 * All the children classes should implement their own way of creating {@link Mask}.
 *
 * @author Mikhail Artemev
 */
public abstract class FormatWatcher implements MaskFactory, ohos.agp.components.Text.TextObserver {

    private DiffMeasures diffMeasures = new DiffMeasures();

    private Mask mask;
    private Text Text;
    private boolean initWithMask;

    private boolean selfEdit = false;
    private boolean noChanges = false;
    private boolean formattingCancelled = false;

    private FormattedTextChangeListener callback;

    protected FormatWatcher() {

    }

    /**
     * Starts to follow text changes in the specified {@link Text} to format any input. <br/>
     * IMPORTANT: this call will force watcher to re-create the mask
     *
     * @param Text text view to watch and format
     */
    public void installOn(final Text Text) {
        installOn(Text, false);
    }

    /**
     * Starts to follow text changes in the specified {@link Text} to format any input.
     * Initial mask's value (e.g. hardcoded head) will be displayed in the view.<br/>
     * IMPORTANT: this call will force watcher to re-create the mask
     *
     * @param Text text view to watch and format
     */
    public void installOnAndFill(final Text Text) {
        installOn(Text, true);
    }

    public void removeFromText() {
        if (Text != null) {
            this.Text.removeTextObserver(this);
            this.Text = null;
        }
    }

    public boolean isInstalled() {
        return this.Text != null;
    }

    public boolean hasMask() {
        return mask != null;
    }

    /**
     * installOn
     *
     * @param Text an observable text view which content text will be formatted using{@link Mask}
     * @param initWithMask this flags defines whether hardcoded head of the mask (e.g "+7 ") will
     *                     fill the initial text of the {@code Text}.
     */
    protected void installOn(final Text Text, final boolean initWithMask) {
        if (Text == null) {
            throw new IllegalArgumentException("text view cannot be null");
        }
        this.Text = Text;
        this.initWithMask = initWithMask;
        Text.removeTextObserver(this);
        Text.addTextObserver(this);
        this.mask = null;
    }

    public void refreshMask() {
        refreshMask(null);
    }

    public void refreshMask(final CharSequence initialValue) {
        final boolean initial = this.mask == null;

        this.mask = createMask();
        checkMask();

        final boolean initiationNeeded = initialValue != null;
        diffMeasures = new DiffMeasures();
        if (initiationNeeded) {
            diffMeasures.setCursorPosition(mask.insertFront(initialValue));
        }

        if ((!initial || initWithMask || initiationNeeded) && isInstalled()) {
            selfEdit = true;
            final String formattedInitialValue = mask.toString();
            Text.setText(formattedInitialValue);
            setSelection(mask.getInitialInputPosition());
            selfEdit = false;
        }
    }

    @Override
    public String toString() {
        return mask == null ? "" : mask.toString();
    }

    @Override
    public void onTextUpdated(String newText, int i, int i1, int i2) {
        if (formattingCancelled || selfEdit || mask == null || noChanges) {
            formattingCancelled = false;
            noChanges = false;
            return;
        }

        String formatted = mask.toString();

        final int cursorPosition = diffMeasures.getCursorPosition();
        if (!formatted.equals(newText)) {
            selfEdit = false;
        }

        if (0 <= cursorPosition && cursorPosition <= newText.length()) {
            setSelection(cursorPosition);
        }
        if (callback != null) {
            callback.onTextFormatted(this, toString());
        }

       // refreshMask();
    }

    /**
     * Unmodifiable wrapper around inner mask. It allows to obtain inner mask statem
     * but not to change it.
     *
     * @return Mask
     */
    public Mask getMask() {
        return new UnmodifiableMask(mask);
    }

    public boolean isAttachedTo(Component view) {
        return this.Text == view;
    }

    public void setCallback(FormattedTextChangeListener callback) {
        this.callback = callback;
    }

    public int getCursorPosition() {
        return diffMeasures.getCursorPosition();
    }

    protected Mask getTrueMask() {
        return mask;
    }

    protected void setTrueMask(Mask mask) {
        this.mask = mask;
    }

    protected Text getText() {
        return Text;
    }

    protected void setText(Text Text) {
        this.Text = Text;
    }

    private void checkMask() {
        if (mask == null) {
            throw new IllegalStateException("Mask cannot be null at this point. Check maybe you forgot " +
                    "to call refreshMask()");
        }
    }

    private void setSelection(int position) {
        if (Text instanceof TextField && position <= Text.length()) {
            // ((TextField) Text).setSelection(position);
        }
    }
}
